import flask
from flask_httpauth import HTTPBasicAuth
import requests
from werkzeug.security import generate_password_hash, check_password_hash
import os

app = flask.Flask(__name__)

auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):

    if os.path.isfile('/etc/flash-auth/.htpasswd'):

        with open('/etc/flash-auth/.htpasswd') as f:
            for line in f:
                username_line = line.split(":", 1)[0]
                password_line = line.split(":", 1)[1].rstrip('\n')
                if username == username_line:
                    print(generate_password_hash(password))
                    return check_password_hash(password_line, password)

        return False
    return True

@app.route('/')
@auth.login_required
def index():

    dependent_response = ''

    if os.environ.get('DEPENDENT_SERVICE_URL') is not None:
        try:
            r = requests.get(url=os.getenv('DEPENDENT_SERVICE_URL','http://localhost'), timeout=5)
            dependent_response = 'Response from ' + os.getenv('DEPENDENT_SERVICE_URL') + '<br>' + r.text
        except:
            dependent_response = 'Cannot connect to ' + os.getenv('DEPENDENT_SERVICE_URL')

    return 'Hello! ' + 'Is a ' + os.getenv('SERVICE_TITLE','unknown') + ' service<br>' + dependent_response

app.run(debug=True, host='0.0.0.0')